package com.tokopedia.testproject.problems.algorithm.waterJug;

import android.util.Log;

public class Solution {

    public static int minimalPourWaterJug(int jug1, int jug2, int target) {
        // TODO, return the smallest number of POUR action to do the water jug problem
        // below is stub, replace with your implementation!
        int a = -1;
        int b = -1;
        int result = 0;

        if(canBeMeasured(jug1, jug2, target)){
            a = pour(jug1, jug2, target);
            result = a;
        }

        if(canBeMeasured(jug2, jug1, target)){
            b = pour(jug2, jug1, target);
            result = b;
        }

        if(a != -1 && b != -1) result = Math.min(a,b);

        Log.i("MinPour", String.valueOf(result));

        return result;
    }

    private static int pour(int from, int to, int target){
        // Initialize current amount of water
        // in source and destination jugs
        int f = from;
        int t = 0;

        // Initialize count of steps required
        int counter = 0; // Needed to fill "from" Jug

        // Break the loop when either of the two
        // jugs has d litre water
        while (f != target && t != target){
            // Find the maximum amount that can be
            // poured
            int temp = Math.min(f, to - t);

            // Pour "temp" litres from "from" to "to"
            t += temp;
            f -= temp;

            // Increment count of pouring
            counter++;

            if (f == target || t == target) break;

            // If first jug becomes empty, fill it
            if (f == 0) f = from;

            // If second jug becomes full, empty it
            if (t == to) t = 0;

        }
        return counter;
    }

    private static boolean canBeMeasured(int x, int y, int z) {
        return z == 0 || (z <= x + y && z % gcd(x, y) == 0);
    }

    private static int gcd(int a, int b) {
        return b==0 ? a : gcd(b, a%b);
    }
}
