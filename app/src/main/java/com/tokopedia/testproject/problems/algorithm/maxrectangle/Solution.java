package com.tokopedia.testproject.problems.algorithm.maxrectangle;

import android.util.Log;

import java.util.Stack;

public class Solution {
    public static int maxRect(int[][] matrix) {
        // TODO, return the largest area containing 1's, given the 2D array of 0s and 1s
        // below is stub
        int rowSize = matrix.length;
        if(rowSize == 0) return 0;

        int columnSize = matrix[0].length;
        int result = maxHist(matrix[0]);

        for (int i = 1; i < rowSize; i++){
            for (int j = 0; j < columnSize; j++)
                if (matrix[i][j] == 1) matrix[i][j] += matrix[i - 1][j];
                result = Math.max(result, maxHist(matrix[i]));
        }
        Log.i("MaxRect", String.valueOf(result));

        return result;
    }

    private static int maxHist(int[] row) {
        Stack<Integer> result = new Stack<>();

        int topVal;
        int area;
        int maxArea = 0;
        int i = 0;
        while (i < row.length){
            if (result.empty() || row[result.peek()] <= row[i]) result.push(i++);
            else {
                topVal = row[result.peek()];
                result.pop();
                area = topVal * i;
                if (!result.empty()) area = topVal * (i - result.peek() - 1 );
                maxArea = Math.max(area, maxArea);
            }
        }

        while (!result.empty()) {
            topVal = row[result.peek()];
            result.pop();
            area = topVal * i;
            if (!result.empty()) area = topVal * (i - result.peek() - 1 );
            maxArea = Math.max(area, maxArea);
        }
        return maxArea;
    }
}
