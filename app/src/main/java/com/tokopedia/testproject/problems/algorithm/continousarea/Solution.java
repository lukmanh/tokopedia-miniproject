package com.tokopedia.testproject.problems.algorithm.continousarea;

import android.util.Log;

/**
 * Created by hendry on 18/01/19.
 */
public class Solution {

    private static boolean[][] visited;
    private static int currentArea = 0;
    private static int maxArea = 0;

    public static int maxContinuousArea(int[][] matrix) {
        // TODO, return the largest continuous area containing the same integer, given the 2D array with integers
        // below is stub
        int r = matrix.length;
        int c = matrix[0].length;

        for(int i = 0; i < r; i++){
            for(int j = 0; j < c; j++){
                initVisited(r,c);
                calculateLargestArea(i, j, matrix, matrix[i][j]);
                if(currentArea > maxArea) maxArea = currentArea;
                currentArea = 0;
            }
        }
        Log.i("MaxContinuousArea", String.valueOf(maxArea));
        return maxArea;
    }

    private static void initVisited(int r, int c){
        visited = new boolean[r][c];
        for(int i = 0; i < r; i++)
            for(int j = 0; j < c; j++)
                visited[i][j] = false;
    }

    private static void calculateLargestArea(int x, int y, int[][] matrix, int currentNumber){
        if(x < 0 || y < 0 || x >= matrix.length || y >= matrix[0].length) return;
        if(visited[x][y]) return;
        if(matrix[x][y] != currentNumber) return;
        currentArea++;
        visited[x][y] = true;
        calculateLargestArea(x, y-1, matrix, currentNumber);
        calculateLargestArea(x+1, y, matrix, currentNumber);
        calculateLargestArea(x, y+1, matrix, currentNumber);
        calculateLargestArea(x-1, y, matrix, currentNumber);
    }
}
